package lib.reg.registry;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Thomas
 */
public abstract class Registry<K, V> {
 
    protected final Map<K, V> data;
    
    public Registry(){
        data = new HashMap<>();
    }
    
    public boolean containsKey(K key){
        return data.containsKey(key);
    }
    
    public V get(K key){
        return data.get(key);
    }
}
