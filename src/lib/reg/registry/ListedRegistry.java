package lib.reg.registry;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Thomas
 */
public class ListedRegistry<K, V extends List> extends Registry<K, V> {
    
    public ListedRegistry(){
        super();
    }
    
    public void register(K key, Object value){
        V list;
        
        if (data.containsKey(key)){
            list = data.get(key);
        } else {
            list = (V)new ArrayList<>();
        }
        list.add(value);
        
        data.put(key, list);
    }
    
    public void unregister(K key){
        if (data.containsKey(key)){
            data.remove(key);
        }
    }
    
    public void unregister(K key, Object value){
        if (data.containsKey(key)){
            V list = data.get(key);
            list.remove(value);
            
            data.put(key, list);
        }
    }
}
