package lib.reg.registry;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Thomas
 */
public class MappedRegistry<K, V extends Map> extends Registry<K, V> {
    
    public MappedRegistry(){
        super();
    }
    
    public boolean containsKey(K key, Object outer){
        return data.containsKey(key) && data.get(key).containsKey(outer);
    }
    
    public Object get(K key, Object outer){
        if (data.containsKey(key)){
            V map = data.get(key);
            return map.get(outer);
        }
        return null;
    }
    
    public void register(K key, Object outer, Object inner){
        V map;
        
        if (data.containsKey(key)){
            map = data.get(key);
        } else {
            map = (V)new HashMap<>();
        }
        map.put(outer, inner);
        
        data.put(key, map);
    }
    
    public void unregister(K key){
        if (data.containsKey(key)){
            data.remove(key);
        }
    }
    
    public void unregister(K key, Object outer){
        if (data.containsKey(key)){
            V map = data.get(key);
            map.remove(outer);
            
            data.put(key, map);
        }
    }
}
