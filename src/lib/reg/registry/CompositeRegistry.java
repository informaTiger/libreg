/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.reg.registry;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

/**
 *
 * @author Thomas
 */
public class CompositeRegistry<K, L extends List> extends Registry<K, Map<? super Object, L>>{
    
    public CompositeRegistry(){
        super();
    }
    
    public boolean containsKey(K key, Object outer){
        return data.containsKey(key) && data.get(key).containsKey(outer);
    }
    
    public L get(K key, Object outer){
        if (data.containsKey(key)){
            Map<Object, L> map = data.get(key);
            return map.get(outer);
        }
        return null;
    }
    
    public void register(K key, Object outer, Object inner) {
        Map<Object, L> map;
        L list;
        
        if (data.containsKey(key)){
            map = data.get(key);
            
            if (map.containsKey(outer)){
                list = map.get(outer);
            } else {
                list = (L)new ArrayList<>();
            }
        } else {
            map = new HashMap<Object, L>();
            list = (L)new ArrayList<>();
        }
        list.add(inner);
        map.put(outer, list);
        
        data.put(key, map);
    }
    
    public void unregister(K key){
        if (data.containsKey(key)){
            data.remove(key);
        }
    }
    
    public void unregister(K key, Object outer){
        if (data.containsKey(key)){
            Map<Object, L> map = data.get(key);
            map.remove(outer);
            
            data.put(key, map);
        }
    }
}
